/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tierramedia;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import tierramedia.entidades.Humano;

/**
 *
 * @author David
 */
public class TierraMedia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Humano pepito = new Humano("pepito");
        Humano aragorn = new Humano("Aragorn");
        for(int i = 0; i<6; i++) {
            
            int ataqueDeAragorn = aragorn.ataca();
            int defensaDePepito = pepito.defiende();
            System.out.println("Ataque = " + ataqueDeAragorn);
            System.out.println("Defensa = " + defensaDePepito);

            System.out.println("Vida de pepito = " + pepito.life);
            if (ataqueDeAragorn > defensaDePepito) {
                pepito.life -= 1;
            }
            System.out.println("Vida de pepito = " + pepito.life);
        }
        Humano ganador = pepito;  
        if(pepito.life == 0)
            ganador = aragorn; 
        System.out.println("and the winner is " + ganador.dimeTuNombre());
        

    }
}