/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tierramedia.interfaces;



/**
 *
 * @author david
 */
public interface Guerrero {
    
    public int ataque();
    public boolean huir();
    public int defensa();
    
}
