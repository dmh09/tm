/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tierramedia.entidades;

import java.util.Random;

/**
 *
 * @author David
 */
public class Humano {
    
    public String nombre;
    //public int edad;
    public String[] arrayNombre = new String[7];
    private int[] valoresR = {1,2,3,4};
    public int life = 3;
    
    
    public Humano(String nombre){
        this.introducirNombre(nombre);
    }
    
    public String dimeTuNombre(){
        return nombre;
    }
    
    public void introducirNombre(String nombre){
        //System.out.println("por aquí ha pasado alguien");
        this.nombre = nombre;
    }
    
    public int ataca(){
        Random random = new Random();
        int val = random.nextInt(5);
        while(val == 0){
            val = random.nextInt(5);
        }
        return val;
    }
    
    public int defiende(){
        Random random = new Random();
        int val = random.nextInt(5);
        while(val == 0){
            val = random.nextInt(5);
        }
        return val;
    }
}