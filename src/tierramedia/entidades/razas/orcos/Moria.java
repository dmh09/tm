/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tierramedia.entidades.razas.orcos;

import practica3.david.entidades.fisicas.Orco;

/**
 *
 * @author david
 */
public class Moria extends Orco{

    private final boolean soportaSol = false;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
     
    
}
