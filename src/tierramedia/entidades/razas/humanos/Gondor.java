/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tierramedia.entidades.razas.humanos;

/**
 *
 * @author david
 */
public class Gondor extends Humano{
    
    public String name;
    public final int habilidadCaballo = 2;
    public final int habilidadGuerrero = 4;
    public final int maxEdad = 250;
    
    public Gondor(int edad) throws Exception{
        super.edad = edad;
        this.CompruebaEdad();
    }
    
    private void CompruebaEdad() throws Exception{
      if(super.edad > this.maxEdad){
          throw new Exception("edad excepcion");
      }   
    }

    public int getEdad() {
        return super.edad;
    }
    
}
